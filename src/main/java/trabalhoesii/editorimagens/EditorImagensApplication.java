package trabalhoesii.editorimagens;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EditorImagensApplication {

    public static void main(String[] args) {
        SpringApplication.run(EditorImagensApplication.class, args);
    }

}
